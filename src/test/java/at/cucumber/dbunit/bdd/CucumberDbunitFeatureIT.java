package at.cucumber.dbunit.bdd;


import at.cucumber.dbunit.bdd.glue.CucumberDbunitGlueDe;
import at.cucumber.dbunit.bdd.glue.CucumberDbunitGlueEn;
import cucumber.runtime.arquillian.CukeSpace;
import cucumber.runtime.arquillian.api.Features;
import cucumber.runtime.arquillian.api.Glues;
import org.h2.tools.RunScript;
import org.h2.tools.Server;
import org.junit.runner.RunWith;

import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.DriverManager;

@RunWith(CukeSpace.class)
@Features({"src/test/resources/cucumber/cucumberDbunitDe.feature", "src/test/resources/cucumber/cucumberDbunitEn.feature"})
@Glues({CucumberDbunitGlueDe.class, CucumberDbunitGlueEn.class})
public class CucumberDbunitFeatureIT {

    private static final String TCP_PORT = "-tcpPort";
    private static final String PORT_NUMBER = Integer.toString(9192);
    private static final String TCP_ALLOW_OTHERS = "-tcpAllowOthers";
    private static final String DB_URL = "jdbc:h2:tcp://localhost:9192/~/cucumberdbunit";
    private static final String USER = "sa";
    private static final String PASSWORD = "";

    private Server server;

    @org.junit.Before
    public void initH2Db() throws Exception {
        this.server = Server.createTcpServer(TCP_PORT, PORT_NUMBER, TCP_ALLOW_OTHERS).start();
        Class.forName("org.h2.Driver");
        Connection conn = DriverManager.getConnection(DB_URL, USER, PASSWORD);
        System.out.println("Connection Established: " + conn.getMetaData().getDatabaseProductName() + "/" + conn.getCatalog());

        RunScript.execute(DB_URL, USER, PASSWORD, "src/test/resources/initH2.sql", StandardCharsets.UTF_8, false);
    }

    @org.junit.After
    public void shutdownH2Db() {
        // stop the TCP Server
        this.server.stop();
    }
}
