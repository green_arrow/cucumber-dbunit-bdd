# language: de

Funktionalität: dieses feature file ist ein test fuer dbunit

  Grundlage:
    Angenommen der Treiber ist org.h2.Driver
    Und die URL ist jdbc:h2:tcp://localhost:9192/~/cucumberdbunit
    Und der Username ist sa
    Und das Passwort ist leer

  Szenario: Es soll ueberprueft werden das einzelene Daten in eine Datenbank gespeichert werden koennen
    Angenommen in der Tabelle PERSON sind folgende Daten vorhanden
      | ID                                   | VORNAME | NACHNAME | GEBURTSDATUM | NATIONALITAET |
      | 4107991a-fd8c-4cd4-8429-e98b4f3ca291 | Anja    | K.       | 1992-07-31   | DEUTSCHLAND   |
    Dann ist die Tabelle PERSON in folgendem Zustand
      | ID                                   | VORNAME | NACHNAME | GEBURTSDATUM | NATIONALITAET |
      | 4107991a-fd8c-4cd4-8429-e98b4f3ca291 | Anja    | K.       | 1992-07-31   | DEUTSCHLAND   |

  Szenario: Es soll ueberprueft werden ob die Datenbank sich in einen definierten Zustand versetzen laesst
    Angenommen die Datenbank befindet sich in folgendem Zustand src/test/resources/cleanAll.xml
    Angenommen in der Tabelle PERSON sind folgende Daten vorhanden
      | ID                                   | VORNAME | NACHNAME | GEBURTSDATUM | NATIONALITAET |
      | 4107991a-fd8c-4cd4-8429-e98b4f3ca291 | Edith   | B.       | 1992-07-31   | DEUTSCHLAND   |
    Dann ist die Tabelle PERSON in folgendem Zustand
      | ID                                   | VORNAME | NACHNAME | GEBURTSDATUM | NATIONALITAET |
      | 4107991a-fd8c-4cd4-8429-e98b4f3ca291 | Edith   | B.       | 1992-07-31   | DEUTSCHLAND   |

  Szenario: Es soll ueberprueft werden ob ein SQL Script in die Datenbank geschrieben werden kann
    Angenommen die Datenbank befindet sich in folgendem Zustand src/test/resources/cleanAll.xml
    Angenommen folgendes SQL Script wird ausgefuehrt /sqlScriptDe.sql
    Dann ist die Tabelle PERSON in folgendem Zustand
      | ID                                   | VORNAME | NACHNAME | GEBURTSDATUM | NATIONALITAET |
      | 22c9b431-0c47-40eb-a0f7-072237867f76 | Silvie  | D.       | 1988-12-12   | TSCHECHIEN    |
      | ebcce661-41a4-45f2-8fa1-ef8152c084c8 | Liya    | S.       | 1999-02-25   | RUSSLAND      |