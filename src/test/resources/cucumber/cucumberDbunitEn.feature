# language: en

Ability: this feature feature file is a test for dbunit

  Background:
    Given is the Driver org.h2.Driver
    Given is the URL jdbc:h2:tcp://localhost:9192/~/cucumberdbunit
    Given is the username sa
    Given is the password empty

  Scenario: test if data can be stored into database
    Given is the table PERSON with following data
      | ID                                   | VORNAME | NACHNAME | GEBURTSDATUM | NATIONALITAET |
      | 4107991a-fd8c-4cd4-8429-e98b4f3ca291 | Anja    | K.       | 1992-07-31   | GERMANY       |
    Then table PERSON is in following state
      | ID                                   | VORNAME | NACHNAME | GEBURTSDATUM | NATIONALITAET |
      | 4107991a-fd8c-4cd4-8429-e98b4f3ca291 | Anja    | K.       | 1992-07-31   | GERMANY       |

  Scenario: test if specific data can be stored into database
    Given is the database in following state src/test/resources/cleanAll.xml
    Given is the table PERSON with following data
      | ID                                   | VORNAME | NACHNAME | GEBURTSDATUM | NATIONALITAET |
      | 4107991a-fd8c-4cd4-8429-e98b4f3ca291 | Edith   | B.       | 1992-07-31   | GERMANY       |
    Then table PERSON is in following state
      | ID                                   | VORNAME | NACHNAME | GEBURTSDATUM | NATIONALITAET |
      | 4107991a-fd8c-4cd4-8429-e98b4f3ca291 | Edith   | B.       | 1992-07-31   | GERMANY       |

  Scenario: test if an sql script can be inserted into the database
    Given is the database in following state src/test/resources/cleanAll.xml
    When the SQL script /sqlScriptEn.sql is processed
    Then table PERSON is in following state
      | ID                                   | VORNAME | NACHNAME | GEBURTSDATUM | NATIONALITAET  |
      | 22c9b431-0c47-40eb-a0f7-072237867f76 | Silvie  | D.       | 1988-12-12   | CZECH REPUBLIC |
      | ebcce661-41a4-45f2-8fa1-ef8152c084c8 | Liya    | S.       | 1999-02-25   | RUSSIA         |