package at.cucumber.dbunit.bdd.glue;

import at.cucumber.dbunit.bdd.dboperations.AbsCucumberDbunitOperations;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * Created by Green Arrow on 20.02.2019.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class CucumberDbunitGlueEn extends AbsCucumberDbunitOperations {

    @Given("^is the Driver (.*)")
    public void isTheDriver(String driver) {
        super.setDriver(driver);
    }

    @Given("^is the URL (.*)$")
    public void isTheUrl(String dbUrl) {
        super.setUrl(dbUrl);
    }

    @Given("^is the username (.*)$")
    public void isTheUsername(String username) {
        super.setUsername(username);
    }

    @Given("^is the password (.*)$")
    public void isThePassword(String password) {
        super.setPassword(password);
    }

    @Given("^is the database in following state (.*)$")
    public void isTheDatabaseInFollowingState(String path) {
        super.setDbStateAccordingToFile(path);
    }

    @Given("^is the table (.*) with following data$")
    public void isTheTableWithFollowingData(String tableName, DataTable dataTable) {
        super.performDbInsertion(tableName, dataTable);
    }

    @Then("^table (.*) is in following state$")
    public void tableIsInFollowingState(String tableName, DataTable table) {
        super.performDbAssertion(tableName, table);
    }

    @When("the SQL script (.*) is processed")
    public void theSQLScriptSqlScriptSqlIsProcessed(String pathToSqlScript) {
        super.insertSqlScriptIntoDatabase(pathToSqlScript);
    }
}
