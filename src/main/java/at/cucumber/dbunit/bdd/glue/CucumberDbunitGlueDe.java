package at.cucumber.dbunit.bdd.glue;

import at.cucumber.dbunit.bdd.dbconnection.DBConnection;
import at.cucumber.dbunit.bdd.dboperations.AbsCucumberDbunitOperations;
import at.cucumber.dbunit.bdd.exception.CucumberDbunitException;
import cucumber.api.DataTable;
import cucumber.api.java.de.Angenommen;
import cucumber.api.java.de.Dann;
import cucumber.api.java.de.Und;
import org.apache.ibatis.jdbc.ScriptRunner;

import java.io.InputStreamReader;
import java.sql.SQLException;

/**
 * Created by Green Arrow on 20.02.2019.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class CucumberDbunitGlueDe extends AbsCucumberDbunitOperations {

    @Angenommen("^der Treiber ist (.*)")
    public void derTreiberIst(String treiber) {
        super.setDriver(treiber);
    }

    @Und("^die URL ist (.*)$")
    public void dieURLIst(String dbUrl) {
        super.setUrl(dbUrl);
    }

    @Und("^der Username ist (.*)$")
    public void derDatenbankUsernameIst(String username) {
        super.setUsername(username);
    }

    @Und("^das Passwort ist (.*)$")
    public void dasPasswortIst(String passwort) {
        super.setPassword(passwort);
    }

    @Angenommen("^die Datenbank befindet sich in folgendem Zustand (.*)$")
    public void dieDatenbankBefindetSichInFolgendemZustand(String path) {
        super.setDbStateAccordingToFile(path);
    }

    @Angenommen("^in der Tabelle (.*) sind folgende Daten vorhanden$")
    public void inDerTabelleSindFolgendeDatenVorhanden(String tableName, DataTable table) {
        super.performDbInsertion(tableName, table);
    }

    @Dann("^ist die Tabelle (.*) in folgendem Zustand$")
    public void istDieTabelleInFolgendmZustand(String tableName, DataTable table) {
        super.performDbAssertion(tableName, table);
    }

    @Angenommen("folgendes SQL Script wird ausgefuehrt (.*)")
    public void folgendesSQLScriptWirdAusgefuehrt(String pathToSqlScript) {
        super.insertSqlScriptIntoDatabase(pathToSqlScript);
    }
}
