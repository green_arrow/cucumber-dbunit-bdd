package at.cucumber.dbunit.bdd.dbconnection;

import at.cucumber.dbunit.bdd.exception.CucumberDbunitException;
import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * Created by Green Arrow on 09.01.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class DBConnection {

    private static Connection JDBC_CONNECTION = null;
    private static IDatabaseConnection connection;

    public static String URL;
    public static String USERNAME;
    public static String PASSWORD;
    public static String DRIVER;

    public static IDatabaseConnection getConnection() {
        if (connection == null) {
            try {
                connection = new DatabaseConnection(getJdbcConnection(), null, true);
            } catch (DatabaseUnitException e) {
                throw new CucumberDbunitException("Could not establish dbunit database connection", e);
            }
        }
        return connection;
    }

    private static Connection getJdbcConnection() {
        if (JDBC_CONNECTION == null) {
            connect();
        }
        return JDBC_CONNECTION;
    }

    private static void connect() {
        try {
            Class.forName(DRIVER);
            JDBC_CONNECTION = DriverManager.getConnection(URL, USERNAME, PASSWORD);
        } catch (Exception e) {
            throw new CucumberDbunitException("Could not establish database connection", e);
        }
    }
}
