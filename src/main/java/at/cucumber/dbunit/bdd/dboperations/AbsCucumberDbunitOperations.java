package at.cucumber.dbunit.bdd.dboperations;

import at.cucumber.dbunit.bdd.dbconnection.DBConnection;
import at.cucumber.dbunit.bdd.dbdata.DbunitDataset;
import at.cucumber.dbunit.bdd.dbdata.DbunitRow;
import at.cucumber.dbunit.bdd.dbdata.DbunitTable;
import at.cucumber.dbunit.bdd.exception.CucumberDbunitException;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import cucumber.api.DataTable;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.jdbc.ScriptRunner;
import org.dbunit.Assertion;
import org.dbunit.dataset.*;
import org.dbunit.dataset.filter.DefaultColumnFilter;
import org.dbunit.dataset.xml.XmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Green Arrow on 09.02.2019.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class AbsCucumberDbunitOperations {

    protected void setDriver(String treiber) {
        DBConnection.DRIVER = treiber;
    }

    protected void setUrl(String dbUrl) {
        DBConnection.URL = dbUrl;
    }

    protected void setUsername(String username) {
        DBConnection.USERNAME = username;
    }

    protected void setPassword(String password) {
        DBConnection.PASSWORD = deEmpty(password);
    }

    /**
     * this method sets a specific database state definied from a file, specially useful when using foreign keys or
     * constraints for resetting the database
     *
     * Attention:
     * in order to delete foreign key constraints in correct order the tables with the foreign constraint
     * need to be deleted before the primary key. DBunit's CLEAN_INSERT takes the dataset in REVERSE ORDER
     * so the tables with the foreign keys neet to be LAST. See:
     * https://stackoverflow.com/questions/26118271/dbunit-fails-to-clean-insert-foreign-key-constraint
     * The problem is that the XMLDataSet I created above does not know the foreign key constraints in the tables,
     * and creates the table list alphabetically. The CLEAN_INSERT operation, however, takes the list of tables and
     * traverses it in reverse order, and it requires that foreign key references (here: ServiceUser.userGroup_id)
     * be deleted before the referenced entity (here: UserGroup).
     *
     * I found this information through Unitils doesn't work and
     * http://forum.spring.io/forum/spring-projects/data/12868-dbunit-test-fails-mysql-server-hates-me?p=337672#post337672
     * Some minor digging into the DbUnit documentation led to a corrected approach to creating the dataset, which checks
     * for foreign key dependencies and tries to order the entities appropriately
     *
     * @param path of the file (beginning with src/test/resources)
     */
    protected void setDbStateAccordingToFile(String path) {
        try {
            IDataSet dataSet = createDbunitDatasetFromXml(new File(path));
            DatabaseOperation.CLEAN_INSERT.execute(DBConnection.getConnection(), dataSet);
        } catch (Exception e) {
            throw new CucumberDbunitException("an error occured in db insertion", e);
        }
    }

    /**
     * this method stores the datatable defined in the feature file into the database
     *
     * @param tableName the correct table name
     * @param table     the datatable containing the data
     */
    protected void performDbInsertion(String tableName, DataTable table) {
        try {
            String datasetAsXml = transformCucumberTableToDbunitXml(tableName, table);
            File tempFile = File.createTempFile("dataset", ".xml");
            FileUtils.writeStringToFile(tempFile, datasetAsXml, StandardCharsets.UTF_8);

            IDataSet dataSet = createDbunitDatasetFromXml(tempFile);
            DatabaseOperation.CLEAN_INSERT.execute(DBConnection.getConnection(), dataSet);
        } catch (Exception e) {
            throw new CucumberDbunitException("an error occured in db insertion", e);
        }
    }

    /**
     * asserts if the giving table definition matches the database state
     * @param tableName the correct table name
     * @param table the datatable containing the data
     */
    protected void performDbAssertion(String tableName, DataTable table) {
        try {
            String xmlDatasetExpected = transformCucumberTableToDbunitXml(tableName, table);
            File tempFile = File.createTempFile("datasetExpected", ".xml");
            FileUtils.writeStringToFile(tempFile, xmlDatasetExpected, StandardCharsets.UTF_8);

            // Expected Data Set
            IDataSet expectedDataSet = createDbunitDatasetFromXml(tempFile);
            ITable expectedTable = expectedDataSet.getTable(tableName);
            ITable sortedExpectedFilteredTable = new SortedTable(expectedTable);

            // Actual Data Set
            IDataSet databaseDataSet = DBConnection.getConnection().createDataSet();
            ITable actualTable = databaseDataSet.getTable(tableName);

            Column[] columns = expectedTable.getTableMetaData().getColumns();

            ITable actualFilteredTable = DefaultColumnFilter.includedColumnsTable(actualTable, columns);
            ITable sortedActualFilteredTable = new SortedTable(actualFilteredTable, columns);

            Assertion.assertEquals(sortedExpectedFilteredTable, sortedActualFilteredTable);
        } catch (Exception e) {
            throw new CucumberDbunitException("an error occured in db assertion", e);
        }
    }

    protected void insertSqlScriptIntoDatabase(String pathToScriptResource) {
        try {
            ScriptRunner scriptRunner = new ScriptRunner(DBConnection.getConnection().getConnection());
            scriptRunner.runScript(new InputStreamReader(this.getClass().getResourceAsStream(pathToScriptResource)));
        } catch (SQLException e) {
            throw new CucumberDbunitException("An error occurde during sql script processing", e);
        }
    }

    /**
     * this method creates from a {@link File} a {@link IDataSet} that can be used by dbunit
     * @param tempFile a file that is created in the temp folder od the OS
     * @return {@link IDataSet}
     * @throws DataSetException
     * @throws FileNotFoundException
     */
    private IDataSet createDbunitDatasetFromXml(File tempFile) throws DataSetException, FileNotFoundException {
        IDataSet dataSet = new XmlDataSet(new FileInputStream(tempFile));
        dataSet = new ReplacementDataSet(dataSet);
        ((ReplacementDataSet) dataSet).addReplacementObject("[null]", null);
        return dataSet;
    }

    /**
     * this method transforms the table defined in a feature file into jackson-based objects
     * @param tableName the correct table name
     * @param table the datatable containing the data
     * @return the XML as {@link String}
     * @throws JsonProcessingException
     */
    private String transformCucumberTableToDbunitXml(String tableName, DataTable table) throws JsonProcessingException {
        // get all table data from cucumber
        List<String> tableHeaders = table.raw().get(0);

        DbunitDataset dbunitDataset = new DbunitDataset();
        DbunitTable dbunitTable = new DbunitTable();
        dbunitTable.setName(tableName);
        dbunitTable.setColumns(tableHeaders);

        List<DbunitRow> dbunitRowList = new LinkedList<>();

        // start with index 1 since index 0 contains only the header names
        for (int i = 1; i < table.raw().size(); i++) {
            dbunitRowList.add(new DbunitRow(table.raw().get(i)));
        }

        dbunitTable.setDbunitRow(dbunitRowList);

        dbunitDataset.setDbunitTable(dbunitTable);
        // transform DbunitObject to xml
        return transformDataTable(dbunitDataset);
    }

    /**
     * this method uses the {@link XmlMapper} of the jackson project to transform the annotated
     * {@link DbunitDataset} into a {@link String}
     * @param dbunitDataset
     * @return
     * @throws JsonProcessingException
     */
    private String transformDataTable(DbunitDataset dbunitDataset) throws JsonProcessingException {
        XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.enable(SerializationFeature.INDENT_OUTPUT);
        xmlMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        xmlMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);

        return xmlMapper.writeValueAsString(dbunitDataset);
    }

    /**
     * transforms empty password to empty {@link String}
     * @param password
     * @return
     */
    private String deEmpty(String password) {
        return StringUtils.equalsAnyIgnoreCase(password, "leer", "empty") ? "" : password;
    }
}
