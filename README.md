# Cucumber-Dbunit-Bdd

This Project was developed to provide a library which allows simple database step definitions for database insertion and table assertion.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Following Software is required to install Cucumber-Dbunit-Bdd locally

```
Build automation tool:          apache-maven-3.3.9
Java Platform:                  jdk1.8.0_181
```

### Usage

1) Download the maven project and install it in your local maven repository with mvn clean install command.
2) Embed it to your own pom.xml file using this:
    ```
    <dependency>
        <groupId>at.greenarrow</groupId>
        <artifactId>cucumber-dbunit-bdd</artifactId>
        <version>***LAST_VERSION***</version>
        <scope>test</scope>
    </dependency>
    ```
3) Integrate the necessary glue class to your JUnit runner. I have tested the library only with cukespace:
    ```
    @RunWith(CukeSpace.class)
    @Features({"src/test/resources/features/XXX.feature"})
    @Glues({CucumberDbunitGlueDe.class, CucumberDbunitGlueEn.class, ...})
    public class ABC  {
        ...
    }
    ```
4) Use step definitions as shown below:
    ```
    # language: en
    
    Ability: this feature feature file is a test for dbunit
    
      Background:
        Given is the Driver org.h2.Driver
        Given is the URL jdbc:h2:tcp://localhost:9192/~/cucumberdbunit
        Given is the username sa
        Given is the password empty
    
      Scenario: test if data can be stored into database
        Given is the table XXXX with following data
        ... do stuff ...
        Then table XXXX is in following state
    ```

## Running the tests

Automated tests are running during maven build. There are examples how to use the german as well as the english version.

## Built With

[Maven 3.3.9](https://maven.apache.org/) - Dependency Management

## Versioning

I use [git](https://gitlab.com/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/green_arrow/moki). 

## Authors

[**Green Arrow**](https://gitlab.com/green_arrow)

[![alt text](https://gitlab.com/uploads/-/system/user/avatar/2597448/avatar.png?width=400 "Green Arrow")](https://gitlab.com/green_arrow)

## License

This project is licensed under the GNU General Public License, Version 3 - see the [LICENSE](LICENSE) file for details